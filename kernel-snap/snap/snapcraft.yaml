# SPDX-License-Identifier: GPL-2.0
# Copyright (C) 2021 Canonical Ltd
name: qti-sa8155p-kernel
summary: Snapdragon kernel for snappy
description: This is a snapdragon snapped kernel, based off msm kernel.lnx.4.14.r18-rel

grade: stable
build-base: core18
confinement: devmode
type: kernel
adopt-info: kernel

architectures:
  - build-on: [amd64, arm64]
    run-on: arm64

# environment to run fde-setup hook
environment:
    SNAP_TA_PATH:    /lib/firmware
    LD_LIBRARY_PATH: ${SNAP_LIBRARY_PATH}:${SNAP}/usr/lib/${SNAPCRAFT_ARCH_TRIPLET}

parts:
  kernel:
    after:
      - libufdt
      - firmware-custom
      - fde-hooks
    plugin: kernel
    source: ${SNAPCRAFT_PROJECT_DIR}/../../../src/kernel/msm-4.14/
    kconfigflavour: "qcomm"
    kconfigs:
      - CONFIG_OF_OVERLAY=n
      - CONFIG_I2C_DEMUX_PINCTRL=n
      - CONFIG_OF_DYNAMIC=n
    kernel-with-firmware: false
    kernel-image-target: Image-dtb
    kernel-compiler-parameters:
      - CC=clang
      - DTC_EXT=dtc
    kernel-initrd-core-base: core20
    kernel-initrd-flavour: edge
    kernel-initrd-compression: gz
    kernel-initrd-firmware:
      - firmware/venus.*
      - firmware/ubuntufde*.mbn
      - firmware/cmnlib*
    kernel-initrd-addons:
      - usr/bin/fde-reveal-key
      - usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/libQseeComApi.*
      - usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/libion.*
      - usr/lib/${SNAPCRAFT_ARCH_TRIPLET}/liblog.*
    override-build: |
      snapcraftctl build
      mkdir -p  ${SNAPCRAFT_PART_INSTALL}/dtbs/
      kernel_version="4.14-perf"
      if [ -d ${KERNEL_DEBUG_ARTIFACTS} ]; then
        cp ${SNAPCRAFT_PART_BUILD}/vmlinux  ${KERNEL_DEBUG_ARTIFACTS}/
        cp ${SNAPCRAFT_PART_BUILD}/.config  ${KERNEL_DEBUG_ARTIFACTS}/
      fi
      # build dtbo image if there are any dtbo files
      if ls ${SNAPCRAFT_PART_BUILD}/arch/arm64/boot/dts/qcom/*.dtbo 1> /dev/null 2>&1; then
          ${SNAPCRAFT_STAGE}/mkdtboimg.py create \
                             ${SNAPCRAFT_PART_INSTALL}/dtbo.img \
                             ${SNAPCRAFT_PART_BUILD}/arch/arm64/boot/dts/qcom/*.dtbo
      fi
      snapcraftctl set-version ${kernel_version}
    stage:
      - System.map-*
      - config-*
      - initrd.img
      - kernel.img
      - modules
      - -modules/*/build
      - -modules/*/source
    prime:
      - -initrd.img
      - -kernel.img

  firmware-custom:
    plugin: dump
    source: .
    source-type: local
    override-pull: |
      # allow custom source definition
      if [ -n "${SNAPDRAGON_KERNEL_SNAP_FIRMWARE:-}" ]; then
        if [ -d ${SNAPDRAGON_KERNEL_SNAP_FIRMWARE} ]; then
          cp -r ${SNAPDRAGON_KERNEL_SNAP_FIRMWARE}/* ${SNAPCRAFT_PART_SRC}
        else
          git clone --depth 1 ${SNAPDRAGON_KERNEL_SNAP_FIRMWARE} ${SNAPCRAFT_PART_SRC}
        fi
      else
        echo "Missing env SNAPDRAGON_KERNEL_SNAP_FIRMWARE no custom firmware will be included"
      fi
      if [ -e "${SNAPCRAFT_PROJECT_DIR}/../../../prebuilts/qti-adreno-firmware.tar" ]; then
          tar -xf ${SNAPCRAFT_PROJECT_DIR}/../../../prebuilts/qti-adreno-firmware.tar -C ${SNAPCRAFT_PART_SRC}
      else
        echo "qti-adreno-firmware.tar is missing from ${SNAPCRAFT_PROJECT_DIR}/../../../prebuilts/"
      fi
    organize:
      'image/': firmware/
      lib/firmware: firmware

  firmware:
    plugin: nil
    stage-packages:
      - linux-firmware
      - wireless-regdb
    override-build: |
      snapcraftctl build
      mkdir -p ${SNAPCRAFT_PART_INSTALL}/firmware/wlan \
               ${SNAPCRAFT_PART_INSTALL}/firmware/smsc75xx \
               ${SNAPCRAFT_PART_INSTALL}/lib
      ln -s ../firmware ${SNAPCRAFT_PART_INSTALL}/lib/firmware
    stage:
      - -firmware/amd
      - -firmware/amdgpu
      - -firmware/asihpi
      - -firmware/cavium
      - -firmware/cxgb3
      - -firmware/cxgb4
      - -firmware/dpaa2
      - -firmware/i915
      - -firmware/imx
      - -firmware/intel
      - -firmware/iwlwifi-*
      - -firmware/liquidio
      - -firmware/matrox
      - -firmware/mellanox
      - -firmware/mrvl
      - -firmware/netronome
      - -firmware/nvidia
      - -firmware/qed
      - -firmware/radeon
      - -firmware/rockchip
      - -firmware/ath10k/QCA9377/hw1.0/firmware-5.bin
      - -firmware/ath3k-1.fw
      - -firmware/qca/htbtfw20.tlv
      - -firmware/qcom/sdm845
      - -firmware/qcom/venus*
    organize:
      lib/firmware: firmware
    prime:
      - -usr
      - -lib

  libufdt:
    plugin: dump
    source: https://android.googlesource.com/platform/system/libufdt
    source-type: git
    source-branch: master
    organize:
      utils/src/mkdtboimg.py: mkdtboimg.py
    stage:
      - mkdtboimg.py
    prime:
      - -*

  # this part if optional, it enables FDE functionality
  fde-hooks:
    plugin: dump
    source: .
    source-type: local
    override-pull: |
      # allow custom source definition
      if [ -d ${SNAPCRAFT_PROJECT_DIR}/../../../prebuilts/ubuntu-fde-prime/ ]; then
          cp -r ${SNAPCRAFT_PROJECT_DIR}/../../../prebuilts/ubuntu-fde-prime/* ${SNAPCRAFT_PART_SRC}
      else
        echo "FDE_HOOK_BUNDLE UNAVAILABLE, FDE functionality won't be included"
      fi
    organize:
      lib/firmware/: firmware/
      usr/bin/fde-setup: meta/hooks/fde-setup
    stage:
      # clean any test bins from reference build
      - -meta/snap.yaml
      - -snap
      - -usr/bin/snapctl
      - -usr/bin/test-fde-setup-reveal
      - -usr/bin/fde-key-manager

  bootimg:
    plugin: nil
    after:
      - kernel
    override-build: |
      mkbootimg \
          --kernel ${SNAPCRAFT_STAGE}/kernel.img \
          --ramdisk ${SNAPCRAFT_STAGE}/initrd.img \
          --output ${SNAPCRAFT_PART_INSTALL}/boot.img.nonsecure \
          --pagesize 4096 \
          --base 0x80000000 \
          --kernel_offset 0x8000 \
          --ramdisk_offset 0x1000000 \
          --tags_offset 0x100 \
          --cmdline 'console=tty0 console=ttyMSM0,115200n8 panic=-1 clk_ignore_unused pd_ignore_unused snapd_lk_boot_disk=sde androidboot.hardware=qcom androidboot.memcg=1 video=vfb:640x400,bpp=32,memsize=3072000 androidboot.usbcontroller=a600000.dwc3 firmware_class.path=/lib/firmware/'
    prime:
      - -*

  signing-test-keys:
    plugin: dump
    source: https://git.launchpad.net/~ubuntu-cervinia/+git/cervinia-test-keys
    source-type: git
    organize:
      '*': signing-keys/
    prime:
      - -*

  sign-bootimg:
    plugin: nil
    after:
      - bootimg
      - signing-test-keys
    override-build: |
      KEY_NAME="bootimg-key"
      BOOT_IMG="boot.img"
      TARGET_SHA_TYPE=sha256
      BOARD_KERNEL_PAGESIZE=2048
      # create signature block
      openssl \
        dgst \
        -${TARGET_SHA_TYPE} \
        -binary ${SNAPCRAFT_STAGE}/${BOOT_IMG}.nonsecure \
        > ${BOOT_IMG}.${TARGET_SHA_TYPE}
      openssl \
        pkeyutl \
        -sign \
        -in ${BOOT_IMG}.${TARGET_SHA_TYPE} \
        -inkey ${SNAPCRAFT_STAGE}/signing-keys/${KEY_NAME}.key \
        -out ${BOOT_IMG}.sig \
        -pkeyopt digest:sha256 \
        -pkeyopt rsa_padding_mode:pkcs1

      # append it to the image
      dd if=/dev/zero of=${BOOT_IMG}.sig.padded bs=${BOARD_KERNEL_PAGESIZE} count=1
      dd if=${BOOT_IMG}.sig of=${BOOT_IMG}.sig.padded conv=notrunc
      cat ${SNAPCRAFT_STAGE}/${BOOT_IMG}.nonsecure ${BOOT_IMG}.sig.padded > ${SNAPCRAFT_PART_INSTALL}/${BOOT_IMG}

build-packages:
  - dpkg-dev
  - android-tools-mkbootimg
  - libssl-dev
  - libfdt-dev
  - cpio
  - clang
  - binutils-aarch64-linux-gnu
  - on amd64:
    - gcc-aarch64-linux-gnu
  - else:
    - gcc
